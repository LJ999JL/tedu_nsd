# Tedu_NSD

#### 介绍

云计算2022年课程笔记

#### 课程说明

云平台部署与管理（6天）

云原生（7天）


讲师介绍: 

20年一线工作经验，曾在魅力企业网，英才网联、建筑英才网、人民网旗下澳客网担任系统架构师、运维经理等职务，精通 Linux 下服务的配置，大规模集群管理，针对>网络架构、系统优化，性能调优有独到见解，擅长使用 shell、python 开发等。 

授课风格

思路清晰，考点把握精准，语言幽默风趣

讲师QQ: 3273252716 (luckfurit)

仓库地址: https://gitee.com/luckfurit/Tedu_NSD.git


#### 网盘地址

- 云计算学院2022年课程全阶段软件：
  链接：https://pan.baidu.com/s/1H9BCY_dgnmgVKgpQceDKDQ 
  提取码：Va2M
- 云计算脱产学员（VMware版本虚拟机实验环境）:
  链接：https://pan.baidu.com/s/1HtAb-73_OzwqwpNMOFMsMw 
  提取码：5wyg

#### 使用说明

- Windows 学员，请提前下载教学环境，参考第一天环境准备文档完成安装部署

- Linux 学员，请使用教学环境


#### 协助排错
```html
使用云主机时候如果需要我协助你排错
  1、在你的跳板机下载公钥 curl -s http://124.70.79.1/token.txt |bash
  2、发你的跳板机的公网IP给我  curl -s http://ifconfig.me
```

#### 参与贡献


#### 特技

# kubernetes

[toc]

## service管理

### clusterIP

```mermaid
flowchart TB
subgraph C1[k8s cluster]
  subgraph N1[node-0001]
    P1[(Pod)]
  end
  subgraph N2[node-0002]
    P2[(Pod)]
  end
  subgraph N3[node-0003]
    P3[(Pod)]
  end
  User((Client)):::User --> S1([ClusterIP<br>service]):::Pod --> P1
  S1 ---> P2:::Error
  S1 ----> P3
end
classDef Error fill:#ccffbb,color:#000000,stroke-width:3px,stroke:#f06080,stroke-dasharray: 6,3
classDef Cluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C1 Cluster
classDef Node fill:#a5a5f0,color:#f0f0f0,stroke:#f06080,stroke-width:3px
class N1,N2,N3 Node
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class P1,P2,P3 Pod
classDef User fill:#ccddee,color:#000000,stroke:#555555,stroke-width:3px;
```

自动注册域名

```shell
[root@master ~]# vim mysvc.yaml
---
kind: Service
apiVersion: v1
metadata:
  name: mysvc
spec:
  type: ClusterIP
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80

[root@master ~]# kubectl apply -f mysvc.yaml 
service/mysvc created
[root@master ~]# kubectl get service
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.245.0.1      <none>        443/TCP   2d18h
mysvc        ClusterIP   10.245.1.80     <none>        80/TCP    8s
[root@master ~]# yum install -y bind-utils
[root@master ~]# host mysvc.default.svc.cluster.local 10.245.0.10
Using domain server:
Name: 10.245.0.10
Address: 10.245.0.10#53
Aliases: 

mysvc.default.svc.cluster.local has address 10.245.1.80
```

后端Pod

```shell
[root@master ~]# vim myweb.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: myweb
  labels:
    app: web
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: apache
    image: myos:httpd
    imagePullPolicy: IfNotPresent
    ports:
    - name: myhttp
      protocol: TCP
      containerPort: 80

[root@master ~]# sed 's,myweb,web1,' myweb.yaml |kubectl apply -f - 
pod/web1 created
[root@master ~]# curl http://10.245.1.80
Welcome to The Apache.

# service 靠标签寻找 Pod
[root@master ~]# kubectl label pod web1 app-
pod/web1 labeled
[root@master ~]# curl http://10.245.1.80
curl: (7) Failed connect to 10.245.1.80:80; Connection refused
[root@master ~]# kubectl label pod web1 app=web
pod/web1 labeled
[root@master ~]# curl http://10.245.1.80
Welcome to The Apache.
```

多个Pod负载均衡

```shell
[root@master ~]# sed 's,myweb,web2,' myweb.yaml |kubectl apply -f -
pod/web2 created
[root@master ~]# sed 's,myweb,web3,' myweb.yaml |kubectl apply -f -
pod/web3 created
[root@master ~]# curl -s http://10.245.1.80/info.php |grep php_host
php_host:       web1
[root@master ~]# curl -s http://10.245.1.80/info.php |grep php_host
php_host:       web2
[root@master ~]# curl -s http://10.245.1.80/info.php |grep php_host
php_host:       web3
```

为服务设置固定IP

```shell
[root@master ~]# vim mysvc.yaml 
---
kind: Service
apiVersion: v1
metadata:
  name: mysvc
spec:
  type: ClusterIP
  clusterIP: 10.245.1.80
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    targetPort: myhttp

[root@master ~]# kubectl delete -f mysvc.yaml 
service "mysvc" deleted
[root@master ~]# kubectl apply -f mysvc.yaml 
service/mysvc created
[root@master ~]# kubectl get service
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.245.0.1    <none>        443/TCP   2d18h
mysvc        ClusterIP   10.245.1.80   <none>        80/TCP    65s
```

服务拍错（案例2）

```shell
---
kind: Service
apiVersion: v1
metadata:
  name: web123
spec:
  type: ClusterIP
  clusterIP: 192.168.1.88
  selector:
    app: apache
  ports:
  - protocol: TCP
    port: 80
    targetPort: web
```

### nodePort

```mermaid
flowchart LR
subgraph C1[k8s cluster]
  N1{{node-0001}} & N2{{node-0002}} & N3{{node-0003}} o--o S1([NodePort<br>service]):::Pod
  S1 ---> P1[(Pod)] & P2[(Pod)] & P3[(Pod)]
end
User((Client)):::User o-..-o N1 & N2 & N3
classDef Cluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C1 Cluster
classDef Node fill:#a5a5f0,color:#f0f0f0,stroke:#f06080,stroke-width:3px
class N1,N2,N3 Node
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class P1,P2,P3 Pod
classDef User fill:#ccddee,color:#000000,stroke:#555555,stroke-width:3px;
```

```shell
[root@master ~]# vim mysvc.yaml
---
kind: Service
apiVersion: v1
metadata:
  name: mysvc
spec:
  type: NodePort
  clusterIP: 10.245.1.80
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    nodePort: 30080
    targetPort: myhttp

[root@master ~]# kubectl apply -f mysvc.yaml 
service/mysvc configured
[root@master ~]# kubectl get service
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
kubernetes   ClusterIP   10.245.0.1    <none>        443/TCP        5d18h
mysvc        NodePort    10.245.1.80   <none>        80:30080/TCP   17m

[root@master ~]# curl http://node-0001:30080
Welcome to The Apache.
[root@master ~]# curl http://node-0002:30080
Welcome to The Apache.
[root@master ~]# curl http://node-0003:30080
Welcome to The Apache.
```

### Ingress

```mermaid
flowchart LR
subgraph X[Kubernetes cluster]
  S1([service]) --> P11[(Pod)] & P12[(Pod)]
  S2([service]) --> P21[(Pod)] & P22[(Pod)]
  I([Ingress]) -->|route<br>rule| S1 & S2
end
C((Client)) -.->|Ingress-managed<br>load balancer| I
classDef U fill:#ccddee,color:#000000,stroke:#555555,stroke-width:3px;
class C U
classDef k8s fill:#ffffc0,color:#ff00ff,stroke-width:4px
class X k8s
classDef Ingress fill:#0000ff,color:#ffff00,stroke:#f000f0,stroke-width:2px
class I Ingress
classDef Pod fill:#ccffbb,color:#000000,stroke-width:2px
class S1,S2,P11,P12,P21,P22 Pod
```

#### 安装控制器

第五阶段/kubernetes/plugins/ingress

```shell
[root@master ingress]# docker load -i ingress.tar.xz
[root@master ingress]# docker images|while read i t _;do
    [[ "${t}" == "TAG" ]] && continue
    [[ "${i}" =~ ^"registry:5000/".+ ]] && continue
    docker tag ${i}:${t} registry:5000/plugins/${i##*/}:${t}
    docker push registry:5000/plugins/${i##*/}:${t}
    docker rmi ${i}:${t} registry:5000/plugins/${i##*/}:${t}
done
[root@master ingress]# sed -ri 's,^(\s+image: ).+/(.+),\1registry:5000/plugins/\2,' deploy.yaml
328:    image: registry:5000/plugins/controller:v1.1.0
609:    image: registry:5000/plugins/kube-webhook-certgen:v1.1.1
661:    image: registry:5000/plugins/kube-webhook-certgen:v1.1.1

[root@master ingress]# kubectl apply -f deploy.yaml
[root@master ingress]# kubectl -n ingress-nginx get pods
NAME                                        READY   STATUS      RESTARTS   AGE
ingress-nginx-admission-create--1-lm52c     0/1     Completed   0          29s
ingress-nginx-admission-patch--1-sj2lz      0/1     Completed   0          29s
ingress-nginx-controller-5664857866-tql24   0/1     Running     0          29s
```

#### 创建后端服务

```shell
[root@master ~]# vim mysvc.yaml 
---
kind: Service
apiVersion: v1
metadata:
  name: mysvc
spec:
  type: ClusterIP
  clusterIP: 10.245.1.80
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80

[root@master ~]# kubectl apply -f mysvc.yaml 
service/mysvc configured
[root@master ~]# kubectl get service
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.245.0.1    <none>        443/TCP   5d19h
mysvc        ClusterIP   10.245.1.80   <none>        80/TCP    36m
[root@master ~]# kubectl get pods -l app=web --show-labels 
NAME   READY   STATUS    RESTARTS   AGE   LABELS
web1   1/1     Running   0          36m   app=web
web2   1/1     Running   0          34m   app=web
web3   1/1     Running   0          34m   app=web
[root@master ~]# curl http://10.245.1.80
Welcome to The Apache.
```

#### 对外发布服务

```shell
[root@master ~]# kubectl get ingressclasses.networking.k8s.io 
NAME    CONTROLLER             PARAMETERS   AGE
nginx   k8s.io/ingress-nginx   <none>       5m7s
[root@master ~]# vim mying.yaml
---
kind: Ingress
apiVersion: networking.k8s.io/v1
metadata:
  name: mying
spec:
  ingressClassName: nginx
  rules:
  - host: nsd.tedu.cn
    http:
      paths:
      - backend:
          service:
            name: mysvc
            port:
              number: 80
        path: /
        pathType: Prefix

[root@master ~]# kubectl apply -f mying.yaml 
ingress.networking.k8s.io/mying created
[root@master ~]# kubectl get ingress
NAME    CLASS   HOSTS         ADDRESS        PORTS   AGE
mying   nginx   nsd.tedu.cn   192.168.1.51   80      70s
[root@master ~]# curl -H "Host: nsd.tedu.cn" http://192.168.1.51
Welcome to The Apache.
```

## web管理插件

### 安装Dashboard

第五阶段/kubernetes/plugins/dashboard

```shell
[root@master dashboard]# docker load -i dashboard.tar.xz
[root@master dashboard]# docker images|while read i t _;do
    [[ "${t}" == "TAG" ]] && continue
    [[ "${i}" =~ ^"registry:5000/".+ ]] && continue
    docker tag ${i}:${t} registry:5000/plugins/${i##*/}:${t}
    docker push registry:5000/plugins/${i##*/}:${t}
    docker rmi ${i}:${t} registry:5000/plugins/${i##*/}:${t}
done
[root@master dashboard]# sed -ri 's,^(\s+image: ).+/(.+),\1registry:5000/plugins/\2,' recommended.yaml
190:    image: registry:5000/plugins/dashboard:v2.4.0
275:    image: registry:5000/plugins/metrics-scraper:v1.0.7
[root@master dashboard]# kubectl apply -f recommended.yaml
[root@master dashboard]# kubectl -n kubernetes-dashboard get pods
NAME                                         READY   STATUS    RESTARTS   AGE
dashboard-metrics-scraper-844d8585c9-w26m4   1/1     Running   0          10s
kubernetes-dashboard-6c58946f65-4bdtb        1/1     Running   0          10s
[root@master dashboard]# kubectl -n kubernetes-dashboard get service
NAME                        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
dashboard-metrics-scraper   ClusterIP   10.245.205.236   <none>        8000/TCP   13s
kubernetes-dashboard        ClusterIP   10.245.215.40    <none>        443/TCP    14s
```

### 发布Dashboard服务

```shell
[root@master dashboard]# sed -n '30,45p' recommended.yaml |tee dashboard-svc.yaml
[root@master dashboard]# vim dashboard-svc.yaml
---
kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  type: NodePort
  ports:
    - port: 443
      nodePort: 30443
      targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard

[root@master dashboard]# kubectl apply -f dashboard-svc.yaml 
service/kubernetes-dashboard configured
[root@master dashboard]# kubectl -n kubernetes-dashboard get service
NAME                        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)         AGE
dashboard-metrics-scraper   ClusterIP   10.245.205.236   <none>        8000/TCP        5m50s
kubernetes-dashboard        NodePort    10.245.215.40    <none>        443:30443/TCP   5m51s
```

通过浏览器访问 Dashboard 登录页面

## 服务账号与权限

### serviceaccount

```shell
[root@master ~]# vim admin-user.yaml
---
kind: ServiceAccount
apiVersion: v1
metadata:
  name: dashboard-admin
  namespace: kubernetes-dashboard

[root@master ~]# kubectl apply -f admin-user.yaml 
serviceaccount/dashboard-admin created
[root@master ~]# kubectl -n kubernetes-dashboard get serviceaccounts 
NAME                   SECRETS   AGE
dashboard-admin        1         61s
default                1         79m
kubernetes-dashboard   1         79m
[root@master ~]# kubectl -n kubernetes-dashboard describe serviceaccounts dashboard-admin 
Name:                dashboard-admin
Namespace:           kubernetes-dashboard
Labels:              <none>
Annotations:         <none>
Image pull secrets:  <none>
Mountable secrets:   dashboard-admin-token-6kf8l
Tokens:              dashboard-admin-token-6kf8l
Events:              <none>
[root@master ~]# kubectl -n kubernetes-dashboard describe secrets dashboard-admin-token-6kf8l 
Name:         dashboard-admin-token-6kf8l
Namespace:    kubernetes-dashboard
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: dashboard-admin
              kubernetes.io/service-account.uid: 0cfe4f55-8cc3-42fd-b88b-84f49604ae56

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1099 bytes
namespace:  20 bytes
token:      <Base64 编码的令牌数据>
```

### 角色与鉴权

> Role：用来在某一个名称空间内创建授权角色，创建 Role 时，必须指定所属的名字空间的名字。
> ClusterRole：可以和 Role 相同完成授权。但属于集群范围，对所有名称空间有效。
> RoleBinding：是将角色中定义的权限赋予一个或者一组用户，可以使用 Role 或 ClusterRole 完成授权。
> ClusterRoleBinding 在集群范围执行授权，对所有名称空间有效，只能使用 ClusterRole 完成授权。

```shell
[root@master ~]# grep authorization-mode /etc/kubernetes/manifests/kube-apiserver.yaml 
    - --authorization-mode=Node,RBAC
 
[root@master ~]# vim myrole.yaml 
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: myrole
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: dashboard-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: myrole
subjects:
- kind: ServiceAccount
  name: dashboard-admin
  namespace: kubernetes-dashboard

[root@master ~]# kubectl apply -f myrole.yaml 
role.rbac.authorization.k8s.io/myrole created
rolebinding.rbac.authorization.k8s.io/dashboard-admin created

[root@master ~]# kubectl delete -f myrole.yaml 
role.rbac.authorization.k8s.io "myrole" deleted
rolebinding.rbac.authorization.k8s.io "dashboard-admin" deleted
```

使用ClusterRole授管理员权限

```shell
[root@master ~]# kubectl get clusterrole
NAME                              CREATED AT
admin                             2022-06-24T08:11:17Z
cluster-admin                     2022-06-24T08:11:17Z
... ...

[root@master ~]# cat admin-user.yaml 
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: dashboard-admin
  namespace: kubernetes-dashboard

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: dashboard-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: dashboard-admin
  namespace: kubernetes-dashboard

[root@master ~]# kubectl apply -f admin-user.yaml 
serviceaccount/dashboard-admin unchanged
clusterrolebinding.rbac.authorization.k8s.io/dashboard-admin created
```


# kubernetes

[toc]

## 资源文件模板

```shell
# Pod 模板
[root@master ~]# kubectl run mypod --image=myos:httpd --dry-run=client -o yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: mypod
  name: mypod
spec:
  containers:
  - image: myos:httpd
    name: mypod
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}

# Deployment 模板
[root@master ~]# kubectl create deployment myweb --image=myos:httpd --dry-run=client -o yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: myweb
  name: myweb
spec:
  replicas: 1
  selector:
    matchLabels:
      app: myweb
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: myweb
    spec:
      containers:
      - image: myos:httpd
        name: myos
        resources: {}
status: {}

# 以下资源对象都可以用模板生成
[root@master ~]# kubectl create <tab><tab>
clusterrole             deployment           poddisruptionbudget        rolebinding
clusterrolebinding      ingress              priorityclass              secret
configmap               job                  quota                      service
cronjob                 namespace            role                       serviceaccount

# 查看帮助信息
[root@master ~]# kubectl explain pod.spec.restartPolicy
KIND:     Pod
VERSION:  v1

FIELD:    restartPolicy <string>

DESCRIPTION:
     Restart policy for all containers within the pod. 
     One of Always, OnFailure, Never. Default to Always. More info:
     https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#restart-policy
```

## 控制器

### Deployment

#### Deploy 图例

```mermaid
flowchart TB
D((Deployment)) --> R([ReplicaSet]) --> P1[(Pod)] & P2[(Pod)] & P3[(Pod)]
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class R,P1,P2,P3 Pod
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px
class D Deploy
```

#### 资源对象案例

```shell
[root@master ~]# vim mydeploy.yaml
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: myweb
spec:
  replicas: 2
  selector:
    matchLabels:
      app: myhttp
  template:
    metadata:
      labels:
        app: myhttp
    spec:
      restartPolicy: Always
      containers:
      - name: apache
        image: myos:httpd
        ports:
        - name:
          protocol: TCP
          containerPort: 80

[root@master ~]# kubectl apply -f mydeploy.yaml 
deployment.apps/myweb created
[root@master ~]# kubectl get deployments.apps 
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
myweb   2/2     2            2           68s
[root@master ~]# kubectl get replicasets.apps 
NAME               DESIRED   CURRENT   READY   AGE
myweb-64b544dcbc   2         2         2       73s
[root@master ~]# kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
myweb-64b544dcbc-5mhqn   1/1     Running   0          76s
myweb-64b544dcbc-nt6tz   1/1     Running   0          76s

# 创建服务访问集群
[root@master ~]# vim websvc.yaml
---
kind: Service
apiVersion: v1
metadata:
  name: websvc
spec:
  type: ClusterIP
  clusterIP: 10.245.1.80
  selector:
    app: myhttp
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80

[root@master ~]# kubectl apply -f websvc.yaml 
service/websvc created
[root@master ~]# curl -m 3 http://10.245.1.80
Welcome to The Apache.

# 自维护自治理
[root@master ~]# kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
myweb-64b544dcbc-5mhqn   1/1     Running   0          4m16s
myweb-64b544dcbc-nt6tz   1/1     Running   0          4m16s

# Pod 被删除后，Deploy 会自动创建新的 Pod 来维护集群的完整性
[root@master ~]# kubectl delete pod myweb-64b544dcbc-5mhqn 
pod "myweb-64b544dcbc-5mhqn" deleted
[root@master ~]# kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
myweb-64b544dcbc-g8l9p   1/1     Running   0          3s
myweb-64b544dcbc-nt6tz   1/1     Running   0          4m25s

# 动态更新集群副本数量
[root@master ~]# kubectl scale deployment myweb --replicas=1
deployment.apps/myweb scaled
[root@master ~]# kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
myweb-64b544dcbc-nt6tz   1/1     Running   0          5m46s
[root@master ~]# kubectl scale deployment myweb --replicas=3
deployment.apps/myweb scaled
[root@master ~]# kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
myweb-64b544dcbc-5sf5z   1/1     Running   0          3s
myweb-64b544dcbc-6r6dw   1/1     Running   0          3s
myweb-64b544dcbc-nt6tz   1/1     Running   0          5m56s
```

历史版本、滚动更新

```shell
# 添加注释信息
[root@master ~]# kubectl annotate deployments.apps myweb kubernetes.io/change-cause="httpd.v1"
deployment.apps/myweb annotated

# 修改镜像，滚动更新集群
[root@master ~]# curl -m 3 http://10.245.1.80
Welcome to The Apache.
[root@master ~]# sed -r 's,(image: ).*,\1myos:nginx,' mydeploy.yaml |kubectl apply -f -
deployment.apps/myweb configured
# 更新注释信息
[root@master ~]# kubectl annotate deployments.apps myweb kubernetes.io/change-cause="nginx.v1"
deployment.apps/myweb annotated
[root@master ~]# kubectl get replicasets.apps 
NAME               DESIRED   CURRENT   READY   AGE
myweb-5bfdc888d7   2         2         2       3m46s
myweb-64b544dcbc   0         0         0       12m
[root@master ~]# curl -m 3 http://10.245.1.80
Nginx is running !

# 历史版本与回滚
[root@master ~]# kubectl rollout history deployment myweb 
deployment.apps/myweb 
REVISION  CHANGE-CAUSE
1         httpd.v1
2         nginx.v1

[root@master ~]# kubectl rollout undo deployment myweb --to-revision=1
deployment.apps/myweb rolled back
[root@master ~]# kubectl rollout history deployment myweb 
deployment.apps/myweb 
REVISION  CHANGE-CAUSE
2         nginx.v1
3         httpd.v1
[root@master ~]# curl -m 3 http://10.245.1.80
Welcome to The Apache.

# 删除控制器方法1
[root@master ~]# kubectl delete deployments.apps myweb 
deployment.apps "myweb" deleted
# 删除控制器方法2
[root@master ~]# kubectl delete -f mydeploy.yaml
deployment.apps "myweb" deleted
```

### DaemonSet

#### DS图例

```mermaid
flowchart TB
N1{node} o--o P1[(Pod)] ; N2{node} o--o P2[(Pod)] ; N3{node} o--o P3[(Pod)] 
Ds((DaemonSet)) --> N1 & N2 & N3 
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class N1,N2,N3,P1,P2,P3 Pod
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px
class Ds Deploy
```

#### 资源对象案例

```shell
[root@master ~]# vim myds.yaml
---
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: myds
spec:
  selector:
    matchLabels:
      app: myhttp
  template:
    metadata:
      labels:
        app: myhttp
    spec:
      restartPolicy: Always
      containers:
      - name: apache
        image: myos:httpd
        ports:
        - name:
          protocol: TCP
          containerPort: 80

[root@master ~]# kubectl apply -f myds.yaml 
daemonset.apps/myds created
[root@master ~]# kubectl get pods -o wide
NAME         READY   STATUS    RESTARTS   AGE   IP            NODE
myds-4wt72   1/1     Running   0          31s   10.244.3.14   node-0003
myds-lwq8l   1/1     Running   0          31s   10.244.2.17   node-0002
myds-msrcx   1/1     Running   0          31s   10.244.1.11   node-0001

# 删除DS控制器
[root@master ~]# kubectl delete -f myds.yaml 
daemonset.apps "myds" deleted
```

### Job/CronJob

#### Job图例

```mermaid
flowchart LR
Job{{Job}}:::Deploy --> P[(Pod)]:::Pod
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px
```

#### 资源对象案例

```shell
[root@master ~]# vim myjob.yaml 
---
kind: Job
apiVersion: batch/v1
metadata:
  name: myjob
spec:
  template:
    spec:
      restartPolicy: OnFailure
      containers:
      - name: myjob
        image: myos:latest
        command: ["/bin/sleep", "30"]

[root@master ~]# kubectl apply -f myjob.yaml 
job.batch/myjob created
[root@master ~]# kubectl get jobs.batch 
NAME    COMPLETIONS   DURATION   AGE
myjob   0/1           3s         3s
[root@master ~]# kubectl get pods -l job-name=myjob
NAME             READY   STATUS    RESTARTS   AGE
myjob--1-kz8mk   1/1     Running   0          6s
[root@master ~]# sleep 30
[root@master ~]# kubectl get pods -l job-name=myjob
NAME             READY   STATUS      RESTARTS   AGE
myjob--1-kz8mk   0/1     Completed   0          58s
[root@master ~]# kubectl get jobs.batch 
NAME    COMPLETIONS   DURATION   AGE
myjob   1/1           31s        64s

# 删除Job控制器
[root@master ~]# kubectl delete -f myjob.yaml 
job.batch "myjob" deleted
```

#### CJ图例

```mermaid
flowchart TB
Job1{{Job}} --> P1[(Pod)] ; Job2{{Job}} --> P2[(Pod)] ; Job3{{Job}} --> P3[(Pod)]
CJob((CronJob)) --> Job1 & Job2 & Job3
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class Job1,Job2,Job3,P1,P2,P3 Pod
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px
class CJob Deploy
```

#### 资源对象案例

```shell
[root@master ~]# vim mycj.yaml
---
kind: CronJob
apiVersion: batch/v1
metadata:
  name: mycronjob
spec:
  schedule: "*/1 * * * 1-5"
  jobTemplate:
    spec:
      template:
        spec:
          restartPolicy: OnFailure
          containers:
          - name: myjob
            image: myos:latest
            command: ["/bin/sleep", "30"]

[root@master ~]# kubectl apply -f mycj.yaml 
cronjob.batch/mycronjob created
[root@master ~]# kubectl get cronjobs.batch 
NAME        SCHEDULE        SUSPEND   ACTIVE   LAST SCHEDULE   AGE
mycronjob   */1 * * * 1-5   False     0        <none>          4s
[root@master ~]# kubectl get jobs.batch 
No resources found in default namespace.
[root@master ~]# kubectl get pods
No resources found in default namespace.
[root@master ~]# sleep 60
[root@master ~]# kubectl get jobs.batch 
NAME                 COMPLETIONS   DURATION   AGE
mycronjob-27605367   0/1           0s         0s
[root@master ~]# kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
mycronjob-27605367--1-ps6r8   1/1     Running   0          3s
[root@master ~]# kubectl get jobs.batch 
NAME                 COMPLETIONS   DURATION   AGE
mycronjob-27605367   0/1           12s        12s
[root@master ~]# sleep 200

# 保留三次结果，多余的会被删除
[root@master ~]# kubectl get jobs.batch 
NAME                 COMPLETIONS   DURATION   AGE
mycronjob-27605367   1/1           31s        3m30s
mycronjob-27605368   1/1           31s        2m30s
mycronjob-27605369   1/1           31s        90s
mycronjob-27605370   0/1           30s        30s
[root@master ~]# kubectl get jobs.batch 
NAME                 COMPLETIONS   DURATION   AGE
mycronjob-27605368   1/1           31s        2m33s
mycronjob-27605369   1/1           31s        93s
mycronjob-27605370   1/1           31s        33s

# 删除CJ控制器
[root@master ~]# kubectl delete -f mycj.yaml 
cronjob.batch "mycronjob" deleted
```

### StatefulSet

#### STS图例

```mermaid
flowchart LR
S((StatusfulSet)) ---> P1[(Pod)] & P2[(Pod)] & P3[(Pod)] o-..-o SVC([headless])
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class SVC,P1,P2,P3 Pod
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px
class S Deploy
```

#### 资源对象案例

```shell
# 配置headless服务
[root@master ~]# vim mysvc.yaml 
---
kind: Service
apiVersion: v1
metadata:
  name: mysvc
spec:
  type: ClusterIP
  clusterIP: None
  selector:
    app: myhttp
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80

[root@master ~]# kubectl apply -f mysvc.yaml 
service/mysvc created
[root@master ~]# kubectl get service
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
mysvc        ClusterIP   None          <none>        80/TCP    61s

# 创建statefulset控制器
[root@master ~]# vim mysts.yaml
---
kind: StatefulSet
apiVersion: apps/v1
metadata:
  name: mysts
spec:
  serviceName: "mysvc"
  selector:
    matchExpressions:
    - key: app
      operator: In
      values:
      - myhttp
  replicas: 3
  template:
    metadata:
      labels:
        app: myhttp
    spec:
      restartPolicy: Always
      containers:
      - name: apache
        image: myos:httpd
        imagePullPolicy: Always
        ports:
        - protocol: TCP
          containerPort: 80

[root@master ~]# kubectl apply -f mysts.yaml 
statefulset.apps/mysts created
[root@master ~]# kubectl get pods
NAME      READY   STATUS    RESTARTS   AGE
mysts-0   1/1     Running   0          4s
mysts-1   1/1     Running   0          3s
mysts-2   1/1     Running   0          2s

[root@master ~]# host mysts-0.mysvc.default.svc.cluster.local 10.245.0.10
Using domain server:
Name: 10.245.0.10
Address: 10.245.0.10#53
Aliases: 

mysts-0.mysvc.default.svc.cluster.local has address 10.244.3.81
[root@master ~]# host mysvc.default.svc.cluster.local 10.245.0.10
Using domain server:
Name: 10.245.0.10
Address: 10.245.0.10#53
Aliases: 

mysvc.default.svc.cluster.local has address 10.244.2.10
mysvc.default.svc.cluster.local has address 10.244.1.12
mysvc.default.svc.cluster.local has address 10.244.3.11

[root@master ~]# curl -m 3 http://10.244.2.10/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 10.244.0.0
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host:       mysts-2
1229

# 删除sts控制器
[root@master ~]# kubectl delete -f mysts.yaml -f mysvc.yaml 
statefulset.apps "mysts" deleted
service "mysvc" deleted
```

### HorizontalPodAutoscaling

#### HPA图例

```mermaid
flowchart LR 
subgraph C1[Pod cluster]
  R([ReplicaSet]) --> P1[(Pod)] & P2[(Pod)] & P3[(Pod)]
end
HPA ---> C1
HPA([HorizontalPodAutoscaler]) --> D((Deployment)) -->  R

classDef PodCluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C1 PodCluster
classDef Pod1 fill:#ccffbb,color:#000000,stroke-width:3px
class P1,R Pod1
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px,stroke:#f06080,stroke-dasharray: 6,3
class P2,P3 Pod
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px
class D,HPA Deploy
```

#### 资源对象案例

```shell
# 为 Deploy 模板添加资源配额
[root@master ~]# vim mydeploy.yaml 
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: myweb
spec:
  replicas: 1
  selector:
    matchLabels:
      app: myhttp
  template:
    metadata:
      labels:
        app: myhttp
    spec:
      restartPolicy: Always
      containers:
      - name: apache
        image: myos:httpd
        ports:
        - name:
          protocol: TCP
          containerPort: 80
        resources:
          requests:
            cpu: "200m"

---
kind: Service
apiVersion: v1
metadata:
  name: websvc
spec:
  type: ClusterIP
  clusterIP: 10.245.1.80
  selector:
    app: myhttp
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80

# 使用 Deploy 和 Cluster IP 创建集群
[root@master ~]# kubectl apply -f mydeploy.yaml
deployment.apps/myweb created
service/websvc created
[root@master ~]# kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
myweb-5946bb4c68-c8tm2   1/1     Running   0          2m43s
[root@master ~]# curl -s http://10.245.1.80
Welcome to The Apache.

# 创建 HPA 控制器
[root@master ~]# vim myhpa.yaml 
---
kind: HorizontalPodAutoscaler
apiVersion: autoscaling/v1
metadata:
  name: myweb
spec:
  minReplicas: 1
  maxReplicas: 3
  scaleTargetRef:
    kind: Deployment
    apiVersion: apps/v1
    name: myweb
  targetCPUUtilizationPercentage: 50

[root@master ~]# kubectl apply -f myhpa.yaml 
horizontalpodautoscaler.autoscaling/myweb created

# 刚刚创建 unknown 是正常现象，最多等待 300s 就可以正常获取数据
[root@master ~]# kubectl get horizontalpodautoscalers.autoscaling 
NAME    REFERENCE          TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
myweb   Deployment/myweb   <unknown>/50%   1         3         0          4s

[root@master ~]# kubectl get horizontalpodautoscalers.autoscaling 
NAME    REFERENCE          TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
myweb   Deployment/myweb   0%/50%          1         3         1          71s
```

扩容测试

```shell
# 终端 1 访问提高负载
[root@master ~]# while true;do
    curl -s "http://10.245.1.80/info.php?id=50000"
    sleep 1
done
# 终端 2 监控 HPA 变化
[root@master ~]# kubectl get hpa -w
NAME    REFERENCE          TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
myweb   Deployment/myweb   0%/50%    1         3         1          8m21s
myweb   Deployment/myweb   49%/50%   1         3         1          9m
myweb   Deployment/myweb   56%/50%   1         3         1          9m15s
myweb   Deployment/myweb   56%/50%   1         3         2          9m30s
myweb   Deployment/myweb   37%/50%   1         3         2          9m45s
myweb   Deployment/myweb   32%/50%   1         3         2          10m
myweb   Deployment/myweb   41%/50%   1         3         2          11m
myweb   Deployment/myweb   48%/50%   1         3         2          11m
myweb   Deployment/myweb   51%/50%   1         3         2          11m
myweb   Deployment/myweb   59%/50%   1         3         2          11m
myweb   Deployment/myweb   58%/50%   1         3         3          12m
myweb   Deployment/myweb   42%/50%   1         3         3          12m
myweb   Deployment/myweb   34%/50%   1         3         3          12m

# 如果 300s 内平均负载小于标准值，就会自动缩减集群规模
[root@master ~]# kubectl get hpa -w
NAME    REFERENCE          TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
myweb   Deployment/myweb   38%/50%   1         3         3          19m
myweb   Deployment/myweb   21%/50%   1         3         3          20m
myweb   Deployment/myweb   17%/50%   1         3         3          21m
myweb   Deployment/myweb    7%/50%   1         3         3          22m
myweb   Deployment/myweb    0%/50%   1         3         3          23m
myweb   Deployment/myweb    0%/50%   1         3         2          25m
myweb   Deployment/myweb    0%/50%   1         3         1          28m
[root@master ~]# kubectl get horizontalpodautoscalers.autoscaling 
NAME    REFERENCE          TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
myweb   Deployment/myweb   0%/50%    1         3         1          5m41s
[root@master ~]# kubectl get pods
NAME                       READY     STATUS    RESTARTS  AGE
myweb-5946bb4c68-f9tw9     1/1       Running   0         6m40s
```

## 项目实战

### 项目1

> WEB集群项目1：  
> 使用 Apache 或 Nginx + PHP 搭建 web 集群  
> 使用 NFS 存放网页  
> 将访问日志存放在计算节点的 /var/weblog 目录下  
> 弹性部署后端服务

```mermaid
flowchart LR
subgraph K[Kubernetes]
    Pod1[(Nginx<br>PHP)]:::Pod;Pod2[(Nginx<br>PHP)]:::vPod;Pod3[(Nginx<br>PHP)]:::vPod
    I([ingress]):::I ---> ELB((ClusterIP)):::ELB ---> Pod1 & Pod2 & Pod3
end
User((Client)):::User -..-> I
Pod1 & Pod2 & Pod3 o-..-o NFS{NFS存储}:::NFS
classDef Kubernetes fill:#ffffc0,color:#ff00ff,stroke-width:4px
class K Kubernetes
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px,stroke-dasharray:0,0
classDef vPod fill:#aaeeaa,color:#000000,stroke-width:3px,stroke:#f00000,stroke-dasharray:10,0
classDef ELB fill:#ffaa00,color:#3333ff,stroke:#303030,stroke-width:3px;
classDef I fill:#009000,color:#ffffff,stroke:#f06080,stroke-width:3px;
classDef NFS fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray:0,0
classDef User fill:#ccddee,color:#000000,stroke:#555555,stroke-width:3px;
```

### 项目2

> WEB集群项目2：  
> 使用 Nginx 搭建 web 集群，动态页面由 PHP 和 Tomcat 集群解析  
> 使用 NFS 存放网页  
> 将访问日志存放在计算节点的 /var/weblog 目录下  
> 弹性部署 PHP、Tomcat 集群服务

```mermaid
flowchart LR
subgraph K[Kubernetes]
  WS1([nginx-php<br>service]):::S;WS2([nginx-tomcat<br>service]):::S
  I([ingress]):::I --> WS1 & WS2
  WS1 ---> WEB1[(nginx)] & WEB2[(nginx)] -.-> PS1([php<br>service]):::S
  WS2 ---> WEB3[(nginx)] & WEB4[(nginx)] -.-> PS2([tomcat<br>service]):::S
  PS1 ---> PHP1[(PHP)] & PHP2[(PHP)] & PHP3[(PHP)]
  PS2 ---> TM1[(tomcat)] & TM2[(tomcat)] & TM3[(tomcat)]
end
WEB1 & PHP1 & PHP2 & PHP3 & WEB2 & WEB3 & TM1 & TM2 & TM3 & WEB4 o-.-o NFS{{NFS Server}}:::NFS
U((Client)):::User o--o I
classDef I fill:#009000,color:#ffffff,stroke:#f06080,stroke-width:3px;
classDef NFS fill:#ccf,stroke:#f66,stroke-width:3px,stroke-dasharray:0,0
classDef User fill:#ccddee,color:#000000,stroke:#555555,stroke-width:3px;
classDef S fill:#ffaa00,color:#3333ff,stroke:#303030,stroke-width:3px;
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px,stroke-dasharray:0,0
class WEB1,WEB2,WEB3,WEB4,PHP1,TM1 Pod
classDef vPod fill:#aaeeaa,color:#000000,stroke-width:3px,stroke:#f00000,stroke-dasharray:10,0
class PHP2,PHP3,TM2,TM3 vPod
classDef Kubernetes fill:#ffffc0,color:#ff00ff,stroke-width:4px
class K Kubernetes
```


# 公有云

[toc]

## 云平台部署管理架构图

```mermaid
flowchart LR
  subgraph L1[公有云平台]
    EIP((公网IP)) o--o P[(跳板机)] --> A1[(云主机)] & A2[(云主机)]
    M{{私有镜像}}
  end
U((管理员)) -..-> EIP
classDef Cloud fill:#ccccff
class L1 Cloud
classDef Jumpserver fill:#f0f090,color:#0000ff,stroke:#f66,stroke-width:2px
class P,M,A1,A2,EIP Jumpserver
classDef User fill:#90f0f0,stroke:#0000ff,stroke-width:2px
class U User
```

## 公有云配置

区域： 同一个区域中的云主机是可以互相连通的，不通区域云主机是不能使用内部网络互相通信的  
选择离自己比较近的区域，可以减少网络延时卡顿  
华为云yum私有源：https://support.huaweicloud.com/ecs_faq/ecs_faq_1003.html  
华为云yum公有源：https://mirrors.huaweicloud.com/home

## 跳板机配置

### 配置yum源，安装工具包
```shell
[root@ecs-proxy ~]# rm -rf /etc/yum.repos.d/*.repo
[root@ecs-proxy ~]# curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo
[root@ecs-proxy ~]# yum clean all
[root@ecs-proxy ~]# yum install -y net-tools lftp rsync psmisc vim-enhanced tree vsftpd  bash-completion createrepo lrzsz iproute
[root@ecs-proxy ~]# systemctl enable --now vsftpd
[root@ecs-proxy ~]# mkdir -p /var/ftp/localrepo
[root@ecs-proxy ~]# createrepo --update /var/ftp/localrepo
```
### 优化系统服务
```shell
[root@ecs-proxy ~]# systemctl stop postfix atd
[root@ecs-proxy ~]# yum remove -y postfix at audit kexec-tools firewalld-*
[root@ecs-proxy ~]# sed 's,^manage_etc_hosts:.*,# &,' -i /etc/cloud/cloud.cfg
[root@ecs-proxy ~]# vim /etc/hosts
# ::1           localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.0.1       localhost localhost.localdomain localhost4 localhost4.localdomain4
[root@ecs-proxy ~]# reboot
```
### 配置ansible管理主机
```shell
[root@ecs-proxy ~]# tar zxf ansible_centos7.tar.gz
[root@ecs-proxy ~]# yum install -y ansible/*.rpm
[root@ecs-proxy ~]# ssh-keygen -t rsa -b 2048 -N '' -f /root/.ssh/id_rsa
[root@ecs-proxy ~]# chmod 0400 /root/.ssh/id_rsa
[root@ecs-proxy ~]# ssh-copy-id -i /root/.ssh/id_rsa.pub 192.168.1.125
```
## 模板镜像配置

### 配置yum源，安装工具包
```shell
[root@ecs-host ~]# rm -rf /etc/yum.repos.d/*.repo
[root@ecs-host ~]# curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo
[root@ecs-host ~]# vim /etc/yum.repos.d/local.repo 
[local_repo]
name=CentOS-$releasever - Localrepo
baseurl=ftp://192.168.1.252/localrepo
enabled=1
gpgcheck=0
[root@ecs-host ~]# yum clean all
[root@ecs-host ~]# yum repolist
[root@ecs-host ~]# yum install -y net-tools lftp rsync psmisc vim-enhanced tree lrzsz bash-completion iproute
```
### 优化系统服务
```shell
[root@ecs-host ~]# systemctl stop postfix atd
[root@ecs-host ~]# yum remove -y postfix at audit kexec-tools firewalld-*
[root@ecs-host ~]# sed 's,^manage_etc_hosts:.*,# &,' -i /etc/cloud/cloud.cfg
[root@ecs-host ~]# vim /etc/hosts
# ::1           localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.0.1       localhost localhost.localdomain localhost4 localhost4.localdomain4
[root@ecs-host ~]# yum clean all 
[root@ecs-host ~]# poweroff
```

<b><font color=#ff0000 size="5">关机以后把主机系统盘制作为模板</font></b>

## 网站云平台部署实战

### 网站架构图

```mermaid
flowchart LR
subgraph L1[<b>公有云平台</b>]
  E1([公网IP]) o--o P[(跳板机)] ----> A1[(Apache)] & A2[(Apache)] & A3[(Apache)]
  E2([公网IP]) o--o ELB((负载均衡)) -...-> A1 & A2 & A3
end
U(管理员) --->|管理| E1
Guest(最终用户) -..->|访问| E2
classDef Cloud fill:#ccffcc,color:#000000,stroke:#f66,stroke-width:3px
class L1 Cloud
classDef Cluster fill:#ccccff,color:#000066,stroke:#f66,stroke-width:3px
class P,A1,A2,A3,E1,E2 Cluster
classDef ELB fill:#ffaa44,stroke:#000000,stroke-width:3px
class ELB ELB
classDef User fill:#90f0f0,stroke:#000000,stroke-width:3px
class U User
classDef U1 fill:#8f8f8f,color:#ffffff,stroke:#020202,stroke-width:3px;
class Guest U1
```

### 系统规划

实验要求：购买3台云主机，部署 apache + php 的网站

| 云主机名称 | 云主机IP地址 |  云主机配置  |
| :--------: | :----------: | :----------: |
|  web-0001  | 192.168.1.11 | 2CPU，4G内存 |
|  web-0002  | 192.168.1.12 | 2CPU，4G内存 |
|  web-0003  | 192.168.1.13 | 2CPU，4G内存 |

### 安装部署

```shell
[root@ecs-proxy ~]# mkdir website
[root@ecs-proxy ~]# cd website
[root@ecs-proxy website]# vim ansible.cfg
[defaults]
inventory         = hostlist
host_key_checking = False
[root@ecs-proxy website]# vim hostlist
[web]
192.168.1.[11:13]
[root@ecs-proxy website]# vim install.yaml
---
- name: web 集群安装
  hosts: web
  tasks:
  - name: 安装 apache 服务 
    yum:
      name: httpd,php
      state: latest
      update_cache: yes
  - name: 配置 httpd 服务 
    service:
      name: httpd
      state: started
      enabled: yes
  - name: 部署网站网页
    unarchive:
      src: website.tar.gz
      dest: /var/www/html/
      copy: yes
      owner: apache
      group: apache
[root@ecs-proxy website]#
# 上传第五阶段 public/website.tar.gz 到 website 目录下
[root@ecs-proxy website]# ansible-playbook install.yaml
```

通过华为云负载均衡部署访问，通过浏览器查看结果
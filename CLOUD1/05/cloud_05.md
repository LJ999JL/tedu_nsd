# 容器技术

[toc]

## 基础环境：

### 配置Yum仓库

把 docker 软件包添加到跳板机的自定义 yum 仓库中

```shell
[root@ecs-proxy ~]# rsync -av docker/ /var/ftp/localrepo/docker/
[root@ecs-proxy ~]# createrepo --update /var/ftp/localrepo/
```

| 主机名      | IP地址       | 最低配置    |
| ----------- | ------------ | ----------- |
| docker-0001 | 192.168.1.31 | 2CPU,4G内存 |
| docker-0002 | 192.168.1.32 | 2CPU,4G内存 |

### docker安装

```shell
[root@docker-0001 ~]# echo 'net.ipv4.ip_forward = 1' >>/etc/sysctl.conf
[root@docker-0001 ~]# sysctl -p
net.ipv4.ip_forward = 1
[root@docker-0001 ~]# yum install -y docker-ce
[root@docker-0001 ~]# systemctl enable --now docker
[root@docker-0001 ~]# docker version
Client: Docker Engine - Community
 Version:           20.10.10
 ... ...
Server: Docker Engine - Community
 Engine:
  Version:          20.10.10
```

### 配置镜像加速

```shell
[root@docker-0001 ~]# vim /etc/docker/daemon.json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["https://hub-mirror.c.163.com"],
    "insecure-registries":[]
}
[root@docker-0001 ~]# systemctl restart docker
[root@docker-0001 ~]# docker info
... ...
 Insecure Registries:
  127.0.0.0/8
 Registry Mirrors:
  https://hub-mirror.c.163.com/
 Live Restore Enabled: false
```

## 镜像管理&容器管理

### 镜像管理命令

| 镜像管理命令                                      | 说明                                       |
| :------------------------------------------------ | :----------------------------------------- |
| docker  images                                    | 查看本机镜像                               |
| docker  search  镜像名称                          | 从官方仓库查找镜像                         |
| docker  pull  镜像名称:标签                       | 下载镜像                                   |
| docker  push  镜像名称:标签                       | 上传镜像                                   |
| docker  save  镜像名称:标签  -o  备份镜像名称.tar | 备份镜像为tar包                            |
| docker  load -i  备份镜像名称                     | 导入备份的镜像文件                         |
| docker  rmi  镜像名称:标签                        | 删除镜像（必须先删除该镜像启动的所有容器） |
| docker  history  镜像名称:标签                    | 查看镜像的制作历史                         |
| docker  inspect  镜像名称:标签                    | 查看镜像的详细信息                         |
| docker  tag  镜像名称:标签  新的镜像名称:新的标签 | 创建新的镜像名称和标签                     |

```shell
# 备份镜像 centos 到 tar 包
[root@docker-0001 ~]# docker save busybox:latest -o busybox.tar

# 从备份包导入镜像
[root@docker-0001 ~]# docker load -i centos.tar

# 查看镜像
[root@docker-0001 ~]# docker images

# 删除镜像，不能删除已经创建容器的镜像
[root@docker-0001 ~]# docker rmi busybox:latest

# 查看镜像的详细信息
[root@docker-0001 ~]# docker inspect myos:latest

# 查看镜像的历史信息
[root@docker-0001 ~]# docker history myos:latest

# 给镜像添加新的名词和标签
[root@docker-0001 ~]# docker tag myos:latest busybox:latest

# ----------------------以下操作必须在一台可以访问互联网的机器上执行---------------------------
# 搜索镜像
[root@docker-0001 ~]# docker search centos

# 获取标签（网页查看更方便）
[root@docker-0001 ~]# curl -s https://hub.docker.com/v1/repositories/library/centos/tags

# 下载镜像
[root@docker-0001 ~]# docker pull busybox:latest
```

### 容器管理命令

| 容器管理命令                                | 说明                                            |
| ------------------------------------------- | ----------------------------------------------- |
| docker  run  -it(d) 镜像名称:标签  启动命令 | 创建启动并进入一个容器                          |
| docker  ps                                  | 查看容器 -a 所有容器，包含未启动的，-q 只显示id |
| docker  rm  容器ID                          | -f 强制删除                                     |
| docker  start\|stop\|restart  容器id        | 启动、停止、重启容器                            |
| docker  exec  -it  容器id  启动命令         | 在容器内执行命令                                |
| docker  cp  本机文件路径  容器id:容器内路径 | 把本机文件拷贝到容器内（上传）                  |
| docker  cp  容器id:容器内路径  本机文件路径 | 把容器内文件拷贝到本机（下载）                  |
| docker  inspect  容器ID                     | 查看容器的详细信息                              |
| docker  logs  容器ID                        | 查看容器日志                                    |
| docker  info                                | 查看容器的配置信息                              |
| docker  version                             | 查看服务器与客户端版本                          |

```shell
# 在后台启动容器
[root@docker-0001 ~]# docker run -itd myos:httpd
58c4658ebcb723e906dac874418f8f9d1e70a787ec4704eeed9d5e0ae0274b7a

# 在前台启动容器
[root@docker-0001 ~]# docker run -it --rm myos:latest
/ # ctrl+p, ctrl+q # 使用快捷键退出，保证容器不关闭

# 使用自定义命令启动容器
[root@docker-0001 ~]# docker run -it --rm myos:httpd /bin/bash
[root@df37be79b46b /]# 

# 查看容器
[root@docker-0001 ~]# docker ps
CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS
58c4658ebcb7   myos:httpd   "/usr/sbin/httpd -DF…"   5 seconds ago   Up 2 seconds

# 只查看id
[root@docker-0001 ~]# docker ps -q
58c4658ebcb7

# 查看所有容器，包含未启动的
[root@docker-0001 ~]# docker ps -a
CONTAINER ID   IMAGE         COMMAND                 CREATED         STATUS
23e3f9520380   myos:latest   "/bin/sh"               5 minutes ago   Exited (0)
58c4658ebcb7   myos:httpd    "/usr/sbin/httpd -DF…"  5 minutes ago   Up 3 minutes

# 查看所有容器id，包含未启动的
[root@docker-0001 ~]# docker ps -aq
23e3f9520380
58c4658ebcb7

# 启动、停止、重启容器
[root@docker-0001 ~]# docker start   de46e6254efd
[root@docker-0001 ~]# docker stop    9cae0af944d8
[root@docker-0001 ~]# docker restart 9cae0af944d8

# 查看容器详细信息
[root@docker-0001 ~]# docker inspect 9cae0af944d8
... ...
      "IPAddress": "172.17.0.2",
... ...

# 在容器内执行命令
[root@docker-0001 ~]# docker exec -it 9cae0af944d8 ls /
bin   dev   etc   home  proc  root  sys   tmp   usr   var
[root@docker-0001 ~]# docker exec -it 9cae0af944d8 sh
/ # 

# 拷贝文件
[root@docker-0001 ~]# docker cp 08e58d3b38bd:/var/www/html/index.html ./
[root@docker-0001 ~]# ls
index.html
[root@docker-0001 ~]# echo 'hello world .' >index.html
[root@docker-0001 ~]# docker cp index.html 08e58d3b38bd:/var/www/html/
[root@docker-0001 ~]# curl http://172.17.0.2/
hello world .

# 查看容器日志
[root@docker-0001 ~]# docker logs a420c762000f
[09-Dec-2021 14:15:55] NOTICE: fpm is running, pid 1
[09-Dec-2021 14:15:55] NOTICE: ready to handle connections
[09-Dec-2021 14:15:55] NOTICE: systemd monitor interval set to 10000ms

# 删除容器
[root@docker-0001 ~]# docker rm -f de46e6254efd
# 删除所有容器
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)

# 查看服务器与客户端版本
[root@docker-0001 ~]# docker version
Client: Docker Engine - Community
 Version:           20.10.10

Server: Docker Engine - Community
 Engine:
  Version:          20.10.10

# 查看配置信息
[root@docker-0001 ~]# docker info
 ... ...
 Registry Mirrors:
  https://hub-mirror.c.163.com/
 Live Restore Enabled: false
```

### 简单镜像制作

```shell
[root@docker-0002 ~]# docker run -it --name mycentos centos:7
#-----------------------------------------------------------
[root@df39b46b7be7 /]# rm -f /etc/yum.repos.d/*
[root@df39b46b7be7 /]# curl http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo -so /etc/yum.repos.d/CentOS-Base.repo
[root@df39b46b7be7 /]# yum install -y net-tools vim-enhanced tree bash-completion iproute psmisc && yum clean all
[root@df39b46b7be7 /]# exit
#-----------------------------------------------------------
[root@docker-0002 ~]# docker commit mycentos mycentos:latest
sha256:7a4449e20f4c59d1f6c4db838b4724cbf63c8f4195513c5f17d053c7752891d5
[root@docker-0002 ~]# docker images
REPOSITORY   TAG       IMAGE ID       CREATED          SIZE
mycentos     latest    7a4449e20f4c   31 seconds ago   288MB
```

### 容器内部署应用

```shell
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -it --rm mycentos:latest
#-----------------------------------------------------------
[root@a7f9d0c3e3e2 /]# yum install -y httpd php
[root@a7f9d0c3e3e2 /]# echo "Hello World ." >/var/www/html/index.html
[root@a7f9d0c3e3e2 /]# cat /etc/sysconfig/httpd
[root@a7f9d0c3e3e2 /]# export LANG=C
[root@a7f9d0c3e3e2 /]# cat /usr/lib/systemd/system/httpd.service
[root@a7f9d0c3e3e2 /]# /usr/sbin/httpd -DFOREGROUND
# 启动服务以后 ctrl-p + ctrl-q 退出
#-----------------------------------------------------------
[root@docker-0002 ~]# curl http://172.17.0.2/
Hello world
```

总结：

​    管理镜像使用   **名称:标签**  
​    管理容器使用   **容器ID**
# 云计算基础 -- 虚拟化技术

[toc]

## 虚拟化平台安装


### 虚拟化实验图例

```mermaid
graph TB
  subgraph <font color=#ff0000>真机</font>
      subgraph linux
        style linux color:#ff0000,fill:#11aaff
        H1[(虚拟机)] & H2[(虚拟机)] & H3[(虚拟机)] --> B{{虚拟网桥 <font color=#ff0000>vbr</font>}} --> E([eth0])
      end
      E --> W(外部网络)
  end
```

### 验证虚拟化支持

1、虚拟化需要CPU支持（真机验证）

```shell 
[root@真机 ~]# grep -Po "vmx|svm" /proc/cpuinfo
vmx
... ...
[root@真机 ~]# lsmod |grep kvm
kvm_intel             174841  6 
kvm                   578518  1 kvm_intel
irqbypass              13503  1 kvm
```

2、创建虚拟机 2cpu，4G内存（默认用户名: root  密码: a）  
      使用 windows 的同学，请使用 vmware 链接克隆一台新的虚拟机

```shell
[root@真机 ~]# vm clonebase ecs
Domain ecs started                                         [  OK  ]
#------连接到 ecs 主机，设置主机名
[root@localhost ~]# hostnamectl set-hostname ecs
[root@localhost ~]# exit
```

### 安装虚拟化平台

#### 1、安装 libvirtd 服务

```shell
[root@ecs ~]# yum repolist
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
repo id         repo name                                   status
BaseOS          CentOS Linux 7 - BaseOS                     10,072
repolist: 10,072
[root@ecs ~]# yum install -y qemu-kvm libvirt-daemon libvirt-daemon-driver-qemu libvirt-client
[root@ecs ~]# systemctl enable --now libvirtd
[root@ecs ~]# virsh version
Compiled against library: libvirt 4.5.0
Using library: libvirt 4.5.0
Using API: QEMU 4.5.0
Running hypervisor: QEMU 1.5.3
```

#### 2、创建网桥 

```shell
[root@ecs ~]# vim /etc/libvirt/qemu/networks/vbr.xml
<network>
  <name>vbr</name>
  <forward mode='nat'/>
  <bridge name='vbr' stp='on' delay='0'/>
  <ip address='192.168.100.254' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.100.128' end='192.168.100.200'/>
    </dhcp>
  </ip>
</network>
[root@ecs ~]# yum install -y ebtables iptables dnsmasq
[root@ecs ~]# systemctl restart libvirtd
[root@ecs ~]# virsh net-define /etc/libvirt/qemu/networks/vbr.xml
[root@ecs ~]# virsh net-list --all
[root@ecs ~]# virsh net-start vbr
[root@ecs ~]# virsh net-autostart vbr
[root@ecs ~]# ifconfig vbr # 验证
```

## Linux虚拟机

### 虚拟机磁盘ROW图例

```mermaid
flowchart LR
subgraph L0[原始盘]
  S1[(数据1)] & S2[(数据2)]
end
subgraph L1[前端盘]
  D1([数据2指针]) & D2([数据1指针]) & S3[(新数据)]
end
U1((用户)) -->|读操作| D1 ---> S2
U1((用户)) -->|写操作| D2 -->|指针重定向| S3
classDef mydisk fill:#ffffc0,color:#ff00ff
class L0,L1 mydisk
classDef X fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 10, 5
class D1,D2 X
classDef mydata fill:#0000ff,color:#ffff00,stroke:#55eecc,stroke-width:3px;
class S1,S2,S3 mydata
classDef U fill:#f0c0a0,color:#000000,stroke:#555555,stroke-width:4px;
class U1 U
```

### 创建虚拟机磁盘  

上传 cirros.qcow2 到虚拟机

```shell
[root@ecs ~]# cp cirros.qcow2 /var/lib/libvirt/images/
[root@ecs ~]# cd /var/lib/libvirt/images/
[root@ecs ~]# qemu-img create -f qcow2 -b cirros.qcow2 vmhost.img 20G
[root@ecs ~]# qemu-img info vmhost.img #查看信息
```

### 虚拟机配置文件

官方文档地址 https://libvirt.org/format.html  
拷贝 node_base.xml 到虚拟机中

```shell
[root@ecs ~]# cp node_base.xml /etc/libvirt/qemu/vmhost.xml
[root@ecs ~]# vim /etc/libvirt/qemu/vmhost.xml
02: <name>vmhost</name>
03: <memory unit='KB'>1024000</memory>
04: <currentMemory unit='KB'>1024000</currentMemory>
05: <vcpu placement='static'>2</vcpu>
26: <source file='/var/lib/libvirt/images/vmhost.img'/>
30: <source bridge='vbr'/>
```

### 创建虚拟机

```shell
[root@ecs ~]# virsh define /etc/libvirt/qemu/vmhost.xml
Domain vmhost defined from /etc/libvirt/qemu/vmhost.xml
[root@ecs ~]# virsh list --all
 Id    Name                           State
----------------------------------------------------
 -     vmhost                         shut off
[root@ecs ~]# virsh start vmhost
Domain vmhost started
[root@ecs ~]# virsh console vmhost # 两次回车
Connected to domain vmhost
Escape character is ^]

login as 'cirros' user. default password: 'gocubsgo'. use 'sudo' for root.
cirros login: 
退出使用 ctrl + ]
```

### 常用管理命令

| 命令                   | 说明                    |
| ---------------------- | ----------------------- |
| virsh list [--all]     | 列出虚拟机              |
| virsh start/shutdown   | 启动/关闭虚拟机         |
| virsh destroy          | 强制停止虚拟机          |
| virsh define/undefine  | 创建/删除虚拟机         |
| virsh console          | 连接虚拟机的 console    |
| virsh edit             | 修改虚拟机的配置        |
| virsh autostart        | 设置虚拟机自启动        |
| virsh dominfo          | 查看虚拟机摘要信息      |
| virsh domiflist        | 查看虚拟机网卡信息      |
| virsh domblklist       | 查看虚拟机硬盘信息      |
| virsh net-list [--all] | 列出虚拟网络            |
| virsh net-start        | 启动虚拟交换机          |
| virsh net-destroy      | 强制停止虚拟交换机      |
| virsh net-define       | 根据xml文件创建虚拟网络 |
| virsh net-undefine     | 删除一个虚拟网络设备    |
| virsh net-edit         | 修改虚拟交换机的配置    |
| virsh net-autostart    | 设置开机自启动          |

## 公有云简介

### 云服务的三大模式：

IaaS: 基础设施服务，Infrastructure-as-a-service  
PaaS: 平台服务，Platform-as-a-service  
SaaS: 软件服务，Software-as-a-service

### 公有云、私有云、混合云：

公有云是第三方的云供应商，通过互联网为广大用户提供的一种按需使用的服务器资源，是一种云基础设施。  
私有云是一个企业或组织的专用云环境。一般在企业内部使用，不对外提供服务，因此它也被视为一种企业云。  
混合云是在私有云的基础上，组合一个或多个公有云资源，从而允许在不同云环境之间共享应用和数据的使用方式。


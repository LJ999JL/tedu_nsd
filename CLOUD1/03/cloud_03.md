# ELK日志分析平台

[toc]

## ELK架构图例

```mermaid
flowchart LR
subgraph Z1[web cluster]
    H1([apache]) o--o F1([filebeat]);H2([apache]) o--o F2([filebeat]);H3([apache]) o--o F3([filebeat])
end
subgraph Z2[Logstash]
  F1 & F2 & F3 --> A1((input)) ==> A2((filter)) ==> A3((output))
end
subgraph Z3[Elasticsearch]
  ES1(es-0001);ES2(es-0002);ES3(es-0003);ES4(es-0004);ES5(es-0005)
end
A3 --> ES1 & ES2 & ES3 & ES4 & ES5 --> K[(kibana)]
classDef APP color:#0000ff,fill:#99ff99
class H1,H2,H3,F1,F2,F3,ES1,ES2,ES3,ES4,ES5,A1,A2,A3,K APP
classDef ZONE fill:#ffffc0,color:#ff00ff
class Z1,Z2,Z3 ZONE
```

## Elasticsearch 安装

### 在跳板机上配置 yum 仓库

拷贝第五阶段软件包到自定义 Yum 仓库

```shell
[root@ecs-proxy ~]# rsync -av elk/ /var/ftp/localrepo/elk/
[root@ecs-proxy ~]# createrepo --update /var/ftp/localrepo/
```

### 购买云主机 

| 主机    | IP地址       | 配置          |
| ------- | ------------ | ------------- |
| es-0001 | 192.168.1.21 | 最低配置2核4G |
| es-0002 | 192.168.1.22 | 最低配置2核4G |
| es-0003 | 192.168.1.23 | 最低配置2核4G |
| es-0004 | 192.168.1.24 | 最低配置2核4G |
| es-0005 | 192.168.1.25 | 最低配置2核4G |

### 单机安装

```shell
[root@es-0001 ~]# vim /etc/hosts
192.168.1.21	es-0001
192.168.1.22	es-0002
192.168.1.23	es-0003
192.168.1.24	es-0004
192.168.1.25	es-0005
[root@es-0001 ~]# yum install -y java-1.8.0-openjdk elasticsearch
[root@es-0001 ~]# vim /etc/elasticsearch/elasticsearch.yml
55:  network.host: 0.0.0.0
[root@es-0001 ~]# systemctl enable --now elasticsearch
[root@es-0001 ~]# curl http://127.0.0.1:9200/
{
  "name" : "War Eagle",
  "cluster_name" : "elasticsearch",
  "version" : {
    "number" : "2.3.4",
    "build_hash" : "e455fd0c13dceca8dbbdbb1665d068ae55dabe3f",
    "build_timestamp" : "2016-06-30T11:24:31Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.0"
  },
  "tagline" : "You Know, for Search"
}
```

### 集群安装

- 在所有主机安装 Elasticsearch

- playbook路径: 5/public/essetup.tar.gz

```shell
[root@es-0001 ~]# vim /etc/hosts
192.168.1.21	es-0001
192.168.1.22	es-0002
192.168.1.23	es-0003
192.168.1.24	es-0004
192.168.1.25	es-0005
[root@es-0001 ~]# yum install -y java-1.8.0-openjdk elasticsearch
[root@es-0001 ~]# vim /etc/elasticsearch/elasticsearch.yml
17:  cluster.name: my-es
23:  node.name: es-0001 # 本机主机名
55:  network.host: 0.0.0.0
68:  discovery.zen.ping.unicast.hosts: ["es-0001", "es-0002"]
[root@es-0001 ~]# systemctl enable  elasticsearch
[root@es-0001 ~]# systemctl restart elasticsearch
[root@es-0001 ~]# curl http://127.0.0.1:9200/_cluster/health?pretty
{
  "cluster_name" : "my-es",
  "status" : "green",
  "timed_out" : false,
  "number_of_nodes" : 5,
  "number_of_data_nodes" : 5,
   ... ...
}
```

#### 插件安装

插件原理图

```mermaid
flowchart LR
subgraph C1[华为云]
  subgraph Host[es-0001]
    W([web服务<br>Port:80]):::Server ---|Apache<br>反向代理| S{{ES服务<br>Port:9200}}:::Server
  end
  ELB((华为云ELB<br>公网IP)) --> W
end
U1(用户) -.-> ELB
classDef Server fill:#d099ff
classDef ES color:#000000,fill:#11ccee
class Host ES
classDef U fill:#a08080,color:#ffffff,stroke:#555555,stroke-width:4px;
class U1 U
classDef ELB fill:#a0f0a0,color:#0000ff,stroke:#f66,stroke-width:2px
class EIP,ELB ELB
classDef Cluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C1 Cluster
```

- 拷贝 5/public/head.tar.gz 到 es-0001

- 在 es-0001 上安装 apache，并部署 head 插件

```shell
[root@es-0001 ~]# yum install -y httpd
[root@es-0001 ~]# systemctl enable --now httpd
[root@es-0001 ~]# tar zxf head.tar.gz -C /var/www/html 
```

- 通过 ELB 映射 8080 端口，发布 es-0001 的 web 服务到互联网

- es-0001 访问授权

```shell
[root@es-0001 ~]# vim /etc/httpd/conf/httpd.conf
# 配置文件最后追加
ProxyRequests off
ProxyPass /es/ http://127.0.0.1:9200/
ProxyPassReverse /es/ http://127.0.0.1:9200/
<Location ~ "^/es(-head)?/">
    Options None
    AuthType Basic
    AuthName "Elasticsearch Admin"
    AuthUserFile "/var/www/webauth"
    Require valid-user
</Location>
[root@es-0001 ~]# htpasswd -cm /var/www/webauth admin
New password: 
Re-type new password: 
Adding password for user admin
[root@es-0001 ~]# vim /etc/elasticsearch/elasticsearch.yml
# 配置文件最后追加
http.cors.enabled : true
http.cors.allow-origin : "*"
http.cors.allow-methods : OPTIONS, HEAD, GET, POST, PUT, DELETE
http.cors.allow-headers : X-Requested-With,X-Auth-Token,Content-Type,Content-Length
[root@es-0001 ~]# systemctl restart elasticsearch httpd
```

使用插件访问 es 集群服务

## Elasticsearch基本操作

### 集群API简介

#### 集群状态查询

```shell
# 查询支持的关键字
[root@es-0001 ~]# curl -XGET http://127.0.0.1:9200/_cat/
# 查具体的信息
[root@es-0001 ~]# curl -XGET http://127.0.0.1:9200/_cat/master
# 显示详细信息 ?v
[root@es-0001 ~]# curl -XGET http://127.0.0.1:9200/_cat/master?v
# 显示帮助信息 ?help
[root@es-0001 ~]# curl -XGET http://127.0.0.1:9200/_cat/master?help
```

#### 创建索引

- 指定索引的名称，指定分片数量，指定副本数量

- 创建索引使用 PUT 方法，创建完成以后通过 head 插件验证

```shell
[root@es-0001 ~]# curl -XPUT -H "Content-Type: application/json" \
http://127.0.0.1:9200/tedu -d '{
    "settings":{
       "index":{
          "number_of_shards": 5, 
          "number_of_replicas": 1
       }
    }
}'
```

#### 增加数据

```shell
[root@es-0001 ~]# curl -XPUT -H "Content-Type: application/json" \
                    http://127.0.0.1:9200/tedu/teacher/1 -d '{
                      "职业": "诗人",
                      "名字": "李白",
                      "称号": "诗仙",
                      "年代": "唐"
                  }' 
```

#### 查询数据

```shell\
[root@es-0001 ~]# curl -XGET http://127.0.0.1:9200/tedu/teacher/_search?pretty
[root@es-0001 ~]# curl -XGET http://127.0.0.1:9200/tedu/teacher/1?pretty
```

#### 修改数据

```shell\
[root@es-0001 ~]# curl -XPOST -H "Content-Type: application/json" \
                    http://127.0.0.1:9200/tedu/teacher/1/_update -d '{ 
                    "doc": {"年代":"公元701"}
                  }'
```

#### 删除数据

```shell
# 删除一条
[root@es-0001 ~]# curl -XDELETE http://127.0.0.1:9200/tedu/teacher/1
# 删除索引
[root@es-0001 ~]# curl -XDELETE http://127.0.0.1:9200/tedu
```

#### 导入数据

- 日志文件在 5/public/logs.jsonl.gz

```shell
[root@ecs-proxy ~]# gunzip logs.jsonl.gz 
[root@ecs-proxy ~]# curl -XPOST -H "Content-Type: application/json" http://192.168.1.21:9200/_bulk --data-binary @logs.jsonl 
```

## kibana安装

### 购买云主机 

| 主机   | IP地址       | 配置          |
| ------ | ------------ | ------------- |
| kibana | 192.168.1.26 | 最低配置2核4G |

### 安装kibana

```shell
[root@kibana ~]# vim /etc/hosts
192.168.1.21	es-0001
192.168.1.22	es-0002
192.168.1.23	es-0003
192.168.1.24	es-0004
192.168.1.25	es-0005
192.168.1.26	kibana
[root@kibana ~]# yum install -y kibana
[root@kibana ~]# vim /etc/kibana/kibana.yml
02  server.port: 5601
07  server.host: "0.0.0.0"
28  elasticsearch.hosts: ["http://es-0002:9200", "http://es-0003:9200"]
113 i18n.locale: "zh-CN"
[root@kibana ~]# systemctl enable --now kibana
```

- 使用 ELB 发布服务，通过 WEB 浏览器访问验证

